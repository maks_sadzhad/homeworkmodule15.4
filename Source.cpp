#include <iostream>

using namespace std;

void func(int N, int c)
{
	for (int i = 0; i <= N; ++i)
	{
		if (i % 2 == c)
			cout << i << " ";
	}
}

int main()
{
	const int N = 10;
	int c;
	cout << "Enter 1 or  0: ";
	cin >> c;
	func(N, c);
	cout << "\n";

	return 0;


}